import React, { useState, useEffect } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import { makeStyles } from '@material-ui/core/styles';
import notificationService from '../services/notificationService';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2)
    }
  }
}));

export default function Notification() {
  const [notificationState, setNotificationState] = useState({ open: false });

  useEffect(() => {
    const subscription = notificationService.events$.subscribe((notification) =>
      setNotificationState(notification)
    );
    return () => subscription.unsubscribe();
  }, []);

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Snackbar
        open={notificationState.open}
        autoHideDuration={3000}
        onClose={() => notificationService.close()}
        message="successfully booked"
      />
    </div>
  );
}
