import React, { useEffect, useState } from 'react';
import { Grid, Typography, Container, Button, Input } from '@material-ui/core';
import moment from 'moment';
import apiClient from '../services/apiClient';
import bookingDialogService from '../services/bookingDialogService';
import notificationService from '../services/notificationService';

const HomeBooking = (props) => {
  const [checkInState, setCheckInState] = useState();
  const [checkOutState, setCheckOutState] = useState();
  const [totalPriceState, setTotalPriceState] = useState();

  useEffect(() => {
    const price = props.home ? props.home.price : 0;
    const checkInDate = moment(checkInState, 'YYYY-MM-DD');
    const checkOutDate = moment(checkOutState, 'YYYY-MM-DD');
    const nights = checkOutDate.diff(checkInDate, 'days');
    const total = nights * price;
    if (total > 0) {
      setTotalPriceState(total);
    } else {
      setTotalPriceState('');
    }
  }, [checkInState, checkOutState, props]);

  if (!props.home) {
    return <div data-testid="empty"></div>;
  }

  const handleBooking = () => {
    apiClient
      .bookHome(props.home, checkInState, checkOutState)
      .then((response) => {
        bookingDialogService.close();
        notificationService.open();
        console.log(response);
      });
  };

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Typography variant="subtitle1" data-testid="title">
          {props.home ? props.home.title : ''}
        </Typography>
      </Grid>

      <Grid item>
        <Typography variant="subtitle1" data-testid="price">
          {props.home ? props.home.price : ''}
        </Typography>
      </Grid>

      <Grid item container direction="row" spacing={4}>
        <Grid item>
          <Typography variant="body1">Check In Date</Typography>
          <Input
            inputProps={{ 'data-testid': 'check-in' }}
            type="date"
            value={checkInState}
            onChange={(e) => setCheckInState(e.target.value)}
          />
        </Grid>
        <Grid item>
          <Typography variant="body1">Check Out Date</Typography>
          <Input
            inputProps={{ 'data-testid': 'check-out' }}
            type="date"
            value={checkOutState}
            onChange={(e) => setCheckOutState(e.target.value)}
          />
        </Grid>
      </Grid>

      <Grid item style={{ marginTop: '1rem' }}>
        Total:
        <Typography variant="subtitle1" data-testid="total">
          {totalPriceState}
        </Typography>
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          type="button"
          data-testid="book-btn"
          style={{ backgroundColor: '#47809e', color: '#ffffff' }}
          onClick={handleBooking}
        >
          Book Now
        </Button>
      </Grid>
    </Grid>
  );
};

export default HomeBooking;
