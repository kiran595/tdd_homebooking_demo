import { act, fireEvent, getByTestId, render } from '@testing-library/react';
import HomeBooking from './HomeBooking';
import apiClient from '../services/apiClient';
import bookingDialogService from '../services/bookingDialogService';
import notificationService from '../services/notificationService';

let container = null;

const mockedHome = {
  title: 'Test home 1',
  image: 'listing.jpg',
  location: 'Test location 1',
  price: '1'
};

beforeEach(() => {
  container = render(<HomeBooking home={mockedHome} />).container;
});

it('should show title', () => {
  expect(getByTestId(container, 'title').textContent).toBe('Test home 1');
});

it('should show price', () => {
  expect(getByTestId(container, 'price').textContent).toBe('1');
});

it('should show check-in date field', () => {
  expect(getByTestId(container, 'check-in')).toBeTruthy();
});

it('should show check-out date field', () => {
  expect(getByTestId(container, 'check-out')).toBeTruthy();
});

it('should calculate total', () => {
  fireEvent.change(getByTestId(container, 'check-in'), {
    target: { value: '2021-6-11' }
  });

  fireEvent.change(getByTestId(container, 'check-out'), {
    target: { value: '2021-6-15' }
  });

  expect(getByTestId(container, 'total').textContent).toBe('');
});

it('should show "" for invalid dates', () => {
  fireEvent.change(getByTestId(container, 'check-in'), {
    target: { value: '2021-6-11' }
  });

  fireEvent.change(getByTestId(container, 'check-out'), {
    target: { value: '2021-6-9' }
  });

  expect(getByTestId(container, 'total').textContent).toBe('');
});

it('should book home after clicking the book button', async () => {
  jest.spyOn(apiClient, 'bookHome').mockImplementation(() => {
    return Promise.resolve();
  });

  fireEvent.change(getByTestId(container, 'check-in'), {
    target: { value: '2021-6-11' }
  });

  fireEvent.change(getByTestId(container, 'check-out'), {
    target: { value: '2021-6-15' }
  });

  getByTestId(container, 'book-btn').click();
  await act(async () => {});

  //assert that apiClient booked the home
  expect(apiClient.bookHome).toHaveBeenCalled();
});

it('should close the dialog and show notification after booking home', async () => {
  jest
    .spyOn(apiClient, 'bookHome')
    .mockImplementation(() => Promise.resolve('Mocked home booked!'));

  jest.spyOn(bookingDialogService, 'close').mockImplementation(() => {});

  jest.spyOn(notificationService, 'open').mockImplementation(() => {});

  fireEvent.change(getByTestId(container, 'check-in'), {
    target: { value: '2021-6-11' }
  });

  fireEvent.change(getByTestId(container, 'check-out'), {
    target: { value: '2021-6-15' }
  });

  getByTestId(container, 'book-btn').click();

  await act(async () => {});

  expect(bookingDialogService.close).toHaveBeenCalled();
  expect(notificationService.open).toHaveBeenCalled();
});

it('should show empty when no home provided', () => {
  container = render(<HomeBooking home={null} />).container;

  expect(getByTestId(container, 'empty')).toBeTruthy();
});
