import React, { Component } from 'react';
import Box from '@material-ui/core/Box';
import { Grid, Container } from '@material-ui/core';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import { fade, makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block'
    }
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto'
    }
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch'
      }
    }
  },
  linkStyle: {
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  }
}));

const Header = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar
        position="static"
        data-testid="logo"
        style={{ backgroundColor: '#47809e' }}
      >
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
            data-testid="menu"
          >
            <MenuIcon />
          </IconButton>
          {/* <Typography className={classes.title} variant="h6" noWrap>
            Material-UI
          </Typography> */}
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search Homes…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput
              }}
              data-testid="search"
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
          <div style={{ flexGrow: 1 }} />
          <Typography className={classes.linkStyle}>
            <Link href="#" color="inherit">
              Become a host
            </Link>
            <Link href="#" color="inherit">
              Help
            </Link>
            <Link href="#" variant="body2" color="inherit">
              Sign up
            </Link>
            <Link href="#" variant="body2" color="inherit">
              Login
            </Link>
          </Typography>
        </Toolbar>
      </AppBar>
      <Container maxWidth="xl">
        <Grid
          container
          direction="row"
          justify="flex-start"
          spacing={2}
          style={{ paddingTop: '1rem' }}
        >
          <Grid item>
            <Button variant="contained" data-testid="home-type">
              Home type
            </Button>
          </Grid>
          <Grid item>
            <Button variant="contained" data-testid="dates">
              Dates
            </Button>
          </Grid>
          <Grid item>
            <Button variant="contained" data-testid="guests">
              Guests
            </Button>
          </Grid>

          <Grid item>
            <Button variant="contained" data-testid="price">
              Price
            </Button>
          </Grid>
          <Grid item>
            <Button variant="contained" data-testid="rooms">
              Rooms
            </Button>
          </Grid>
          <Grid item>
            <Button variant="contained" data-testid="amenities">
              Amenities
            </Button>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default Header;
