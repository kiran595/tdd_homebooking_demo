import React, { useEffect, useState } from 'react';
import { Grid, Typography, Container, Button } from '@material-ui/core';

import apiClient from '../services/apiClient';
import bookingDialogService from '../services/bookingDialogService';
import Modal from './Modal';
import Notification from './Notification';

import Barcelona from '../assets/images/download.jpeg';

const Homes = () => {
  const [homeState, setHomesState] = useState([]);
  const [bookingDialogState, setBookingDialogState] = useState({ open: false });
  const [selectedHome, setSelectedHome] = useState({});

  const handleClose = () => {
    setBookingDialogState({ open: false });
  };

  useEffect(() => {
    const subscription = bookingDialogService.events$.subscribe((state) =>
      setBookingDialogState(state)
    );
    return () => subscription.unsubscribe();
  }, []);

  useEffect(() => {
    const homesDataPromise = apiClient.getHomes();
    homesDataPromise.then((homesData) => setHomesState(homesData));
  }, []);

  let homes;

  homes = homeState.map((home, index) => {
    return (
      <Grid
        item
        data-testid="home"
        key={index}
        style={{ marginBottom: '3rem', marginLeft: '2rem' }}
      >
        <Grid item>
          <img
            data-testid="home-image"
            src={Barcelona}
            alt="homeimage"
            style={{ width: '200px', height: '200px' }}
          />
        </Grid>
        <Grid item>
          <Typography data-testid="home-title" variant="subtitle1" align="left">
            {home.title}
          </Typography>
        </Grid>

        <Grid item>
          <Typography data-testid="home-location" variant="subtitle1">
            {home.location}
          </Typography>
        </Grid>

        <Grid item>
          <Typography data-testid="home-price" variant="subtitle1">
            ${home.price}
          </Typography>
        </Grid>
        <Grid item>
          <Button
            data-testid="home-booking-btn"
            variant="contained"
            type="button"
            onClick={() => {
              bookingDialogService.open(home);
              setSelectedHome(home);
            }}
            style={{ backgroundColor: '#47809e', color: '#ffffff' }}
          >
            Book Now
          </Button>
        </Grid>
      </Grid>
    );
  });

  return (
    <Container maxWidth="xl">
      <Typography variant="h4">Homes</Typography>
      <Grid container direction="row" style={{ marginTop: '2rem' }}>
        {homes}
      </Grid>
      <Modal
        handleClose={handleClose}
        open={bookingDialogState.open}
        home={selectedHome}
      />
      <Notification />
    </Container>
  );
};

export default Homes;
