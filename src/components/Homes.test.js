import {
  getAllByTestId,
  getByTestId,
  render,
  act,
  getNodeText
} from '@testing-library/react';
import apiClient from '../services/apiClient';
import bookingDialogService from '../services/bookingDialogService';
import Homes from './Homes';

let container = null;

beforeEach(async () => {
  jest.spyOn(apiClient, 'getHomes').mockImplementation(() => {
    return Promise.resolve([
      {
        title: 'Test home 1',
        image: 'listing.jpg',
        location: 'Test location 1',
        price: '1'
      },
      {
        title: 'Test home 2',
        image: 'listing.jpg',
        location: 'Test location 2',
        price: '1'
      },
      {
        title: 'Test home 3',
        image: 'listing.jpg',
        location: 'Test location 3',
        price: '1'
      },
      {
        title: 'Test home 4',
        image: 'listing.jpg',
        location: 'Test location 4',
        price: '1'
      }
    ]);
  });
  container = render(<Homes />).container;
  await act(async () => {});
});

it('should show homes', () => {
  const homes = getAllByTestId(container, 'home');
  expect(homes.length).toBeGreaterThan(0);
});

it('should show homes title', () => {
  const homeTitles = getAllByTestId(container, 'home-title');
  expect(getNodeText(homeTitles[0])).toBe('Test home 1');
});

it('should show homes image', () => {
  const homeImages = getAllByTestId(container, 'home-image');
  expect(homeImages[0]).toBeTruthy();
});

it('should show homes location', () => {
  const homeLocations = getAllByTestId(container, 'home-location');
  expect(getNodeText(homeLocations[0])).toBe('Test location 1');
});

it('should show homes price', () => {
  const homePrice = getAllByTestId(container, 'home-price');
  expect(getNodeText(homePrice[0])).toBe('$1');
});

it('should show home booking button', () => {
  jest.spyOn(bookingDialogService, 'open').mockImplementation(() => {});
  const homeBookingBtn = getAllByTestId(container, 'home-booking-btn');
  homeBookingBtn[0].click();
  expect(bookingDialogService.open).toHaveBeenCalledWith({
    title: 'Test home 1',
    image: 'listing.jpg',
    location: 'Test location 1',
    price: '1'
  });
});
