const apiClient = {
  getHomes: () => {
    return fetch(
      'https://run.mocky.io/v3/62de12a6-dce1-4b9c-a34c-c77e275df98a'
    ).then((response) => response.json());
  },

  bookHome: (home, checkIn, checkOut) => {
    return fetch(
      'https://run.mocky.io/v3/2716381d-260c-4ab5-b303-9bce60d292cf'
    ).then((response) => response.json());
  }
};

export default apiClient;
