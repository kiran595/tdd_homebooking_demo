import logo from './logo.svg';
import './App.css';
import Button from '@material-ui/core/Button';
import { Header, Homes } from './components';

function App() {
  return (
    <div className="App">
      <Header />
      <Homes />
    </div>
  );
}

export default App;
